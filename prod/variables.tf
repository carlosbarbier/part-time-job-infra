variable "region" {
  description = "Aws region"
  type        = string
  default     = ""
}
variable "AWS_ACCESS_KEY_ID" {
  description = "api db user"
  type = string
}

variable "AWS_SECRET_ACCESS_KEY" {
  description = "aws secret key"
  type = string
}

variable "COGNITO_USER_CLIENT_ID" {
  description = "cognito user client id"
  type = string
}

variable "REGION_NAME" {
  description = "region name "
  type = string
}




