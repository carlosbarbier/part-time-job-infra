variable "region" {
  description = "Aws region"
  type        = string
}
variable "AWS_ACCESS_KEY_ID" {
  description = "aws access id"
  type = string
}

variable "AWS_SECRET_ACCESS_KEY" {
  description = "aws secret key"
  type = string
}

variable "COGNITO_USER_CLIENT_ID" {
  description = "cognito user client id"
  type = string
}

variable "dynamo_tables" {
  type = list(string)
  default = ["S_USERS","S_JOBS","S_CATEGORIES"]
}